# A PROPOS

Le projet *epnconnect* consiste à décrire un protocole interopérable d'échange entre un module logiciel installé sur un poste client et un progiciel de gestion d'espace public numérique.

La description et le code de ce projet sont placés sous licences libres.

Ce projet a été écrit initialement pour converser avec cybermin version de Martigues puis par cybergestionnaire, tous deux des forks de cybermin.

N'hésitez pas à contribuer au développement de ce logiciel libre sur la [forge de développement](https://git.framasoft.org/epnaconnect). 
Les discussions autour des améliorations à apporter au logiciel ont lieu sur les [forums animepn](http://animepn.openphpbb.com/f16-epnconnect).


# HISTOIRE

Ce projet a été écrit initialement pour converser avec [cybermin version de Martigues](http://cybermin.martigues.free.fr/index.php?m=3)
par [ses auteurs](http://cybermin.martigues.free.fr/index.php?m=1) puis utilisé dans [cybergestionnaire](http://maxletesteur.jimdo.com/cyber-gestionnaire/), tous deux des forks de [cybermin](http://cybermin.free.fr/index.php?m=1).

ctariel a écrit une [autre version](https://github.com/ctariel/LGB-Connect/).


# CONTRIBUTEURS

- Brice SAINT MARTIN, version d'origine
- ctariel, autre version
- Loïc DAYOT
- florenced alias maxletesteur

